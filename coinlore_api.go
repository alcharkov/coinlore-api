package coinlore_api

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

//Get JSON by currency ID
func GetDataById(id string) string {
	response, err := http.Get(fmt.Sprintf("https://api.coinlore.com/api/ticker/?id=%s", id))
	if err != nil {
		log.Println(err)
		return ""
	}

	data, err := ioutil.ReadAll(response.Body)
	defer response.Body.Close()
	if err != nil {
		log.Println(err)
		return ""
	}
	return string(data)

}
